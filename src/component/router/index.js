import React from 'react'
import { StyleSheet, Text, View } from 'react-native'
import { createStackNavigator } from '@react-navigation/stack';
import { Home, Splash } from '../page';

const Stack = createStackNavigator();

const Router = () => {
    return (
        <Stack.Navigator>            
            <Stack.Screen name="Splash" component={Splash} options={{headerShown:false}} />
            <Stack.Screen name="Home" component={Home} options={{headerShown:false}}  />
        </Stack.Navigator>
    )
}

export default Router;

const styles = StyleSheet.create({})
