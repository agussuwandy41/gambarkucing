import React, {Component} from 'react'
import { Dimensions, ActivityIndicator, Image, StyleSheet, Text, TouchableOpacity, View } from 'react-native'

export class Home extends Component {
  state={
    dataGambar:'',
    dataGambarWidth:0,
    dataGambarHeight:0,
    showLoader:false,
  }
  showLoader = () => {this.setState({showLoader:true})}
  hideLoader = () => {this.setState({showLoader:false})}

  klikTombol = async () => {
    this.showLoader();
    const url = 'https://api.thecatapi.com/v1/images/search';
    let data = '';

    await fetch(url)
    .then((response)=>response.json())
    .then((json) => {
      data = json
    });

    let gambar = data[0]['url'];
    let lebarGambar = data[0]['width']
    let tinggiGambar = data[0]['height']

    let lebarLayar = Dimensions.get('window').width;
    lebarLayar = lebarLayar - 40;
    lebarLayar = Math.round(lebarLayar);

    let tinggiLayar = (tinggiGambar/lebarGambar) * lebarLayar;
    tinggiLayar = Math.round(tinggiLayar);

    this.setState({dataGambar:gambar});
    this.setState({dataGambarWidth:lebarLayar});
    this.setState({dataGambarHeight:tinggiLayar});
    
    this.hideLoader();
  }
  displayGambar = () => {
    if(this.state.dataGambar){
      return(
        <View style={styles.viewGambar}>
          <Image
            source={{uri:this.state.dataGambar}}
            style={{borderRadius:9,width:this.state.dataGambarWidth, height:this.state.dataGambarHeight}}
          />
        </View>
      )
    }
  }
  render(){
    return (
      <View style={styles.container}>
        {this.displayGambar()}
        <View style={styles.viewTombol}>
          <TouchableOpacity style={styles.tombol} onPress={this.klikTombol}>
            <Text style={styles.teks}>Tampilkan Gambar Kucing!</Text>
          </TouchableOpacity>
        </View>
        <View
          style={{position:'absolute', top:'50%',right:0, left:0}}>
          <ActivityIndicator animating={this.state.showLoader} size="large" color="red"/>
        </View>
      </View>
    )
  }
}
export default Home;

const styles = StyleSheet.create({
  container :{
    flex:1,
    padding:20,
  },
  viewGambar:{
    flex:4,
  },
  viewTombol:{
    flex:1,
    justifyContent:'center',
    alignItems:'center'
  },
  tombol:{
    width:'95%',
    height:70,
    backgroundColor:'green',
    borderRadius:20,
    justifyContent:'center',
    alignItems:'center',
    elevation:4,
  },
  teks:{
    color:'white',
    fontSize:35,
  }
})
